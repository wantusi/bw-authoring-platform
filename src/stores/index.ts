import { defineStore } from "pinia";
import { base } from "@/services";
console.log(base);
const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [],
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage();
      this.$patch({
        navList: data[0].filter((item: any) => item.id),
      });
    },
  },
});
export default useStore;
