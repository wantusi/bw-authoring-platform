import BaseHeader from "@/components/baseHeader/BaseHeader.vue";
import { App } from "vue";
import icon from "@/components/iconFont/index";
export default {
  install(app: App): void {
    app.component("base-header", BaseHeader);
    Object.keys(icon).forEach((key) => {
      app.component(key, icon[key]);
    });
  },
};
